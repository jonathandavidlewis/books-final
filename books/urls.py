from django.urls import path
from books.views import(
    delete_book,
    show_books,
    create_book,
    show_book,
    delete_book,
    update_book,
    list_reviews
)

urlpatterns = [
    path('reviews/', list_reviews, name="list_reviews"),
    path('', show_books, name="list_books"),
    path('create/', create_book, name="create_book"),
    path('<int:pk>/', show_book, name="book_detail"),
    path('<int:pk>/delete', delete_book, name="delete_book"),
    path('<int:pk>/update', update_book, name="update_book"),
]

# we need to register books URLs inside the main URLs file