### Defines functions which are invoked when the user visits  a certain URL

# this is going to allow us to send a response to the user
from django.http import HttpResponse
from django.shortcuts import render
# we need the render function from django.shortcuts to return html templates back to the user's browswer


# request has an information about the http request made by the user
def homepage(request):
    # return HttpResponse("homepage")  # It will simplpy return a string to browser page

    # Now instead of returning a simple string, we are going to return a template after commenting out line 19

    # In order to render something, we need to import the render function in line 5
    return render(request,"homepage.html")
    # To return the template, you need to go to settings.py and add the templates directory path to ''DIRS': ['./book_inventory_project/templates'], property
